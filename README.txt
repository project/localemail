*******************************************************
  README.txt for LocalEmail.module for Drupal
*******************************************************

Local Email allows users to log in with out an external email address. Optionally, they can use a local email address of the form @. The module has the following features:

- When enabled removes email fields from new user registration and my account pages for non-admins
- Can automatically assign an email address of the form username@domain.
- Allows you to set the domain used in the administration interface.
- Optionally, allows you to not store any email information at all.
- Allows you to use a security question to allow users to reset their forgotten password.
- Security questions can be edited through the admin interface.
- When security questions are enabled it replaces the user/password reset form with

Future releases may support the ability to run a script which actually creates the local email in the email server of your choice.

One advantage of this module is that no "identifying information" is collected. This MAY help you avoid complications with the FCC when dealing with users who are under 13 years old.

How to Install?
=================

1) Upload the module.
2) Go to admin/build/modules. Enable LocalEmail module.
3) Goto admin/settings/localemail. Check the box, in case, you do not want to show the welcome message, account and sign out box when the user logs in.
4) Don't forget to turn-off email-verification from: admin/user/settings by unchecking the checkbox that says: "Require e-mail verification when a visitor creates an account".

Local Email was written by Joseph Coffland August 2007.
Modified by Saurabh (Ebizon Technologies) www.ebizontek.com on January 6, 2009.


